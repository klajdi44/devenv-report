# Development environment report 


- ## Table of contents
	- [Disposition](#Disposition)
	- [Requirements](#Requirements)
	- [Containerization](#containerization)
	- [Software Quality Assurance](#software-quality-assurance)
	- [Version Control System](#version-control-system)
	- [Database System](#database-system)
	- [Operating System Components](#operating-system-components)
	- [Why Django](#why-django)
  
<br/>

## Disposition
This report is written as part of the exam for the course Development Environments at the Web Development BA program at KEA (Copenhagen School of Technology and Design)

**It is written by**:
* Jonas Bøgh
* Klajdi Ajdini
* Stefan-Andrei Mihutoni

The task given was to set up a development environment for a web application. For this project we have chosen a Todo application built in the Back End with Python & Django course as the starting point.

The aim of this report is to guide you through our process and considerations of setting up this environment and to describe how we have addressed the requirements for it.

We will document how we containerized the app, ensuring that the application runs in an isolated environment with all project dependencies. We will describe which measures we took regarding software quality assurance. Both from a static point of view, meaning that the application code format lives up to the same standard between all developers on the team, but also how we handle testing the functionality of the code, which requires the code to be executed. Furthermore, we will describe how we have dealt with version control in this project including our use of branches during the project development. Lastly, we will evaluate the technology stack used to build this Todo application. 

To start the project in development mode, run the following command:

```
RTE=dev docker-compose up
```

<br/>

## Requirements
The requirements for this project will be regarding the development environment aspect. Therefore we came up with the following requirements, which we will address during the report.

* The project must run the same regardless the operating system
* It must be easy to find other developers in case of growth of business
* Code written by many developers should live up to an agreed standard
* We need to address the security of the application.
* Code that has bugs should fail when a pull request is made
* Code that could cause potential errors should automatically not be allowed to be merged
* We need to make sure that we can test the functionality of the code we write, before deployment.
* We need to be able to run the project in different settings depending on the runtime environment. 

<br/>

## Containerization
* We have used Docker to containerize the application. The setup consists of three containers.
* A web server – running Nginx
* An application server – running our Django project
* A database server – running PostgreSQL

![Apps]('./../public/apps.png')

The purpose of containerizing the app is to build an isolated environment where the application runs. By doing that we can install all the project dependencies and run the application inside a container rather than relying on what is installed on the host. 

Furthermore, we can run multiple containers simultaneously and make them interact with each other. This makes sure that the application runs in the same environment regardless of the host running it and ensures consistency when the project is run on different hosts.

We can start and stop all containers with one command and to make that work, we need a set of files in our project directory.
* Dockerfile - to build the image for our django application
* docker-compose - to spin up all containers with one command
* `Entrypoint.sh` - to check the runtime environment and run the required commands for each case.
* Env file
  
<br/>

**Dockerfile**  
The starting point of the containerization process is our Dockerfile which is located in the root of the project directory. The Dockerfile describes the actions we need to take to make our Django application work. 
```
FROM python:3.9-slim-buster 
ENV PYTHONUNBUFFERED=1
ENV PYTHONDONTWRITEBYTECODE=1
 
RUN apt-get update && \
	apt-get install -y libpq-dev python3-dev python-dev python-psycopg2 python3-psycopg2 gcc

ADD requirements.txt /
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
WORKDIR /app
COPY . /app
COPY ./entrypoint.sh /
ENTRYPOINT ["sh", "entrypoint.sh"]
```

The FROM directive specifies the base image we use. Since Django is a python web framework, we start with a python: 3.9-slim-buster image.
The first RUN directive installs a set of dependencies for this project to work.
The ADD directive takes the requirements.txt file, which documents our project dependencies, from the project directory and adds it to the root directory of the container.
The following two RUN commands will upgrade the installation tool pip and install every dependency from the requirements.txt file.

Finally, we copy a shell file, `entrypoint.sh`,  from our project directory into the root directory of the container and execute it.

<br/>

**Entrypoint**
```
#!/bin/sh
 
echo "** RTE mode: ${RTE} - Running entrypoint"
 
case "$RTE" in
	dev )
    	echo "** Development mode."
    	python manage.py check
    	python manage.py makemigrations --merge
    	python manage.py migrate --noinput
    	pip-audit
    	coverage run --source="." --omit=manage.py manage.py test --verbosity 2
    	python manage.py runserver 0.0.0.0:8000
    	;;
	test )
    	echo "** Test mode."
    	pip-audit || exit 1
    	python manage.py check
    	python manage.py makemigrations
    	python manage.py migrate
    	pip install coverage
    	coverage run --source="." --omit=manage.py manage.py test --verbosity 2
    	coverage report -m --fail-under=65
    	;;
	prod )
    	echo "** Production mode."
    	pip-audit || exit 1
    	python manage.py check --deploy
    	python manage.py collectstatic --noinput
    	gunicorn todo_project.asgi:application -b 0.0.0.0:8080 -k uvicorn.workers.UvicornWorker
    	;;
esac
```

When we start the project, we must specify a runtime environment (the RTE variable). The RTE can have three different values:
* dev – for setting up the project in a development mode
* test – for performing test cases and generate a coverage report
* prod – for deploying the project.

This report will only focus on the dev and test case.

<br/>

### Running the project in development mode:
When running the project in development mode, we will take the following steps:
* we will run a sanity check on the Django codebase and the configuration (check)
* we will update and synchronize our database according to our models (makemigrations, migrate)
* we will look for project dependencies with known security vulnerabilities (pip audit)
* we will start a development server (runserver) where we map port 0.0.0.0 inside the container to port 8000 outside.

<br/>

### Running the project in test mode:
When we run the project in test mode, we do many of the same steps as we do in development mode, regarding setting up the project, checking the project and auditing the project.
The difference compared to the development mode is, that rather than starting a development server, we will:
* Install coverage, a reporting tool when we perform our test cases
* we perform our test case
* generate a test report and specify a minimum value (in percent) for code coverage to succeed.

<br/>

**docker-compose**
```
version: "3.8"
 
services:
 db:
   image: postgres:14-alpine
   volumes:
     - db_data:/var/lib/postgresql/data/
   env_file:
     - db_${RTE}.env
 
 app:
   # yamllint disable-line rule:line-length
   image: registry.gitlab.com/jona71e2/django-docker-ci-exam/app:latest
   ports:
     - 8000:8000
     - 8080:8080
   env_file:
     - db_${RTE}.env
   volumes:
     - .:/app/
     - media:/media/
     - static:/static/
   depends_on:
     - db

 nginx:
   build: nginx/
   ports:
     - 443:443
     - 80:80
   volumes:
     # - ./nginx/${RTE}/conf.d/:/etc/nginx/conf.d/
     # - ./certs/:/etc/letsencrypt/
     - media:/media/
     - static:/static/
   depends_on:
     - app
 
volumes:
 db_data:
 media:
 static:
```

<br/>

Using docker-compose we can define multiple containers (services) for our project, which can be started/stopped using just one command. 
As described earlier, our project consists of three containers, which we have listed under services.
* db
* app
* nginx


**Database - db service:**
For our database container to run, we need three parts:
* An image - in this case a postgres:14-alpine image.
* A volume - to persist data between start and stop.
* Env file - to connect to the database container. The env file is dependent on the runtime environment specified when starting the project.

**Application - app service:**
The application service needs the following components to run.
* An image - based on the Dockerfile described earlier. Instead of building the image locally, we do that on the Gitlab CI-pipeline, which will be explained later. So the image is stored in the Gitlab image registry.
* Port mapping: We specify two ports which are port 8000 and port 8080 and mapping both of them to the exact same ports that are inside the docker container.
* Env file - access to the same env file as the database container, so that the application container can access the database container. It is still dependent on the runtime environment.
* Volumes:
     * First we map the project into the app directory in the container. This is convenient when developing the app, so that changes to our project directory outside of the container are reflected in the container.
     * Media and static are for deployment which we will not go into detail with in this report.
* We make this container depend on the database container, which means that the database container needs to be up and running before the application container starts.

**Web Server - nginx service:**
* Image: The nginx service builds an image based on a Dockerfile located in the nginx directory. This image uses an nginx:alpine, installs certbot, which is related to deployment and adds a scheduler.txt file and an entrypoint file into the container and executes the entrypoint.
* The nginx container depends on the application container to start. 

<br/>

## Software Quality Assurance

In terms of software quality assurance, there are a number of things that we were concerned about and wanted to address.
* The coding standard
* Testing the code

<br/>

### Linting
Code standards enforced by automated rule checks improve the readability and maintainability of code - as well as reduce the number of bugs. Set standards helped us establish self-improvement routines and healthy habits to follow. 

By setting up the same coding standard between all developers we ensure that everybody works using the same syntax and by that we avoid potential conflicts when different developers merge their code together. 

To solve this problem we are using a set of linting tools, to run when we push code to our remote repository. The tools we use for this project is:
* Pylama - for python code
* Djlint - for django specific code
* Shellcheck - for shell scripts
* Yamllint - for yaml files

<br/>

### Testing
The other thing that we were concerned about in terms of software quality assurance was testing the code. To address that, we have written tests using a built-in Django module. Using a tool called coverage, we can run the tests and generate a report to see how much we actually cover in our tests.

At the time of writing this report we have some main tests that test the application,some examples of them are as follows: 
* Url test, this test makes a get call to the root directory and checks it it responds with a status 200
* Main todo app tests that perform the following tests in order to see if everything is functional:
	* Creating a new todo item
	* Changing the status of the newly created todo item 
	* Deleting todo items 
* We also have specific view tests, this following tests are examples of our todo app index view, we test both the GET and the POST HTTP methods: 
```class ViewTests(TestCase):
   def setUp(self) -> None:
       # Create User
       self.client = Client()
       self.user_one = User.objects.create_user(
           username='Pip', email='pip@install.com', first_name='testuser', password='PIP!12345678')
       self.authenticated_user = self.client.login(username=self.user_one.username,
                                                   password='PIP!12345678')
 
   def test_Index_GET(self):
response = self.client.get(reverse('todo_app:index'))
       self.assertEquals(response.status_code, 200)
       self.assertTemplateUsed(response, 'todo_app/index.html')
 
   def test_Index_POST(self):
       response = self.client.post(reverse('todo_app:index'), {
           'text': 'Post Request Todo',
           'user': self.user_one
       })
       saved_todo = Todo.objects.get(text='Post Request Todo')
       todo_text = saved_todo.text
       todo_user = self.user_one.username
 
       self.assertEqual(response.status_code, 200)
       self.assertEqual(todo_text, 'Post Request Todo')
       self.assertEqual(todo_user, 'Pip')
```
There are 3 main parts to this test: 
* The setup, where we setup the correct data needed for all tests 
* The GET test, where we test for a 200 response code and the correct django template 
* The POST test, where we test a post request to the index view with a new todo item and see if that was created successfully or not. 

<br/>

### Continuous Integration - Gitlab CI
The above tests are run whenever someone pushes new code to the repository located on gitlab, if there are any errors then the pipeline will fail, that saves us from any potential bugs.

The way we implemented this part in our project is done as part of a continuous integration pipeline set up on Gitlab.

The setup is based on our .gitlab-ci.yml file in the root of our project directory:
```
stages:
 - static
 - build
 
static:
 stage: static
 image: python:3.10-alpine
 before_script:
   - pip install --upgrade pip
   - pip install -r sqa-requirements.txt
   - apk update && apk add shellcheck
 script:
   - pylama .
   - djlint .
   - pip-audit
   - yamllint *.yml
   - shellcheck *.sh
 
build:
 stage: build
 image: registry.gitlab.com/henrikstroem/composer:latest
 services:
   - docker:dind
 variables:
   DOCKER_DRIVER: overlay2
 before_script:
   - docker login -u gitlab-ci-token -p "$GITLAB_CI_TOKEN" "$CI_REGISTRY"
 script:
   - docker build --pull -t "$CI_REGISTRY_IMAGE/app:latest" .
   - docker push "$CI_REGISTRY_IMAGE/app:latest"
   - RTE=test docker-compose up --abort-on-container-exit --exit-code-from app

```

We have set up two stages in our .gitlab-ci.yml.

* static
* build

The static stage is responsible for linting the code, which means that this stage does not need code execution. All the linter tools will just check if all files in our project directory live up to the syntax of the linters. This is done based on a python:3.10-alpine image. Before we can run the stage we need to install the required software for this. These are documented in the sqa-requirements.txt file. The only other thing we need to install is shellcheck. After this setup, we can run all the linter tools in our project directory.

The build stage is taking care of two things:
* Building and pushing the image for the django application based on the Dockerfile in the project directory
* Running tests on our code

The build stage is where we actually test the code, which means that we need to set up the environment that the app requires for execution. To do that we need to run docker inside docker. For that to work, we use an image provided by Henrik Strøm, with the required dependencies. Furthermore, we need to login to the Gitlab registry before we can actually run the scripts. This first line of  the scripts is building the django-application image and giving it a tag. Next we push the image to the Gitlab registry, and finally we start up all the containers for the application, and run the test inside the django application.

The order of the stages is always that the static stage runs first, and only if that succeeds, then the build stage will run. This makes sense in this case, since there is no reason to start executing code unless the code is approved from a static point of view.

<br/>

**Implementing our self-hosted runner**

Our project is hosted on gitlab, which uses Gitlab Runner to run the CI pipeline jobs. The one hosted on Gitlab is free to use but it’s a shared one and gives us access to 400 minutes for CI/CD purposes.

In our opinion the shared runner has two main caveats: 

1. Only provides 400 minutes of CI which at one point will not be enough for our project;
2. Since it is shared we have to wait in a queue in front of other users using the shared runner, depending on how many users are using the shared runner at the same time, the waiting time could potentially be very high which is not ideal.  

Because of the reasons mentioned above we decided to implement our own runners on our servers. By doing this we eliminated the 2 main drawbacks mentioned above when using a shared runner. We now can have unlimited minutes to run the CI and we don’t have to wait in a queue since our own servers handle everything. 

<br/>

**Installing and running the runner:**

1. Creating the Docker volume:
```
docker volume create gitlab-runner-config
```
We chose to have the runner inside a Docker container rather than on the VPS local system to benefit from the process isolation that docker offers. 

2. Start the GitLab Runner container using the volume we just created:

``` 
docker run -d --name gitlab-runner --restart always \
-v /var/run/docker.sock:/var/run/docker.sock \
```

```
-v gitlab-runner-config:/etc/gitlab-runner \
gitlab/gitlab-runner:latest
```

3. Register the runner 

```
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```
In this step we register our runner with the project by adding a registration token that we have generated from gitlab.

```
docker run --rm -it -v gitlab-runner-config:/etc/gitlab-runner gitlab/gitlab-runner:latest register
```

![Own runner 1]('./public/own-runner1.png')

![Own runner 2]('./public/own-runner2.png')

















## Version Control System
During the development of this project we have used Gitlab as our remote repository. This section will explain our approach of working together as a group using git and Gitlab.

###  Branching strategy
As a branching strategy for our project we have used Feature driven development.
This means creating and developing on branches based on the features in our app.

Main branch: Protected, no one can push to main. Will be updated with merging from development and release branches.

Development: Branch where all our new branches with new features will be merged into.

### Workflow when developing new features
1. Create a new branch for features.
2. Add separate / several commits with the exact changes we are doing at a given time.
3. Push the branch to gitlab.
4. Create a pull request into the development branch and assign reviewers.
5. If CI succeeds then we are allowed to merge into development

### Release cycle
1. Create a new branch from development containing all the features we are working on.
2. The branch has a name of release and a number indicating the version.
E.g. release 0.1, 
3. Make pull requests from that branch to main and assign reviewers.
4. If CI passes we also create tags that help us keep track of versions.
5. Merge branch name to main

In order to keep track of the released cycles, each release is tagged with the version number. 

Example of a “Create” and “Push” tag command for the release version 0.1:
```
git tag -a v0.1 -m '0.1'
git push origin '0.1'
```
In the image below you can see a screenshot from our project’s repository from Gitlab with the tag for the first release:

![Own runner 1]('./public/tag.png')

<br/>

### Git log
Our project development history can be seen in the git log, depicted in the image below:
![Own runner 1]('./public/git-log.png')

<br/>

## Database System
For this project we are using a relational database management system (RDBMS) – more specifically a PostgreSQL database.

The data we need to store in this project is related to our users and to the todos our users create.

The data we need to store about users:
* Username
* First name
* Last name
* Email

The data we need to store about todos:
* User
* Text
* Status

All our users can be described by the same attributes and all todos follow the same structure and can be described by the same attributes. For that reason, a relational database is a great fit, since we can define a User table, storing all the data about our users and a Todo table storing all our todos. Furthermore, we need to filter todos based on the user they are created by, which a RDBMS is really good at.

To sum up – the benefits of using as relational database for this project boils down to:

* A defined database schema – with the tables, attributes, and datatypes we need for our data – helps improve the data integrity.
* Avoiding data redundancy – when normalized properly. We have one source of truth, and this ensures that our data remain in a consistent state.
* The robustness of the RDBMS – it has been around for several years, been tested and developed for improved performance. It can handle huge amounts of data.

The downsides of the relational database paradigm in this project is that it is not as fast setting it up as other kinds of databases. We need to make a database design beforehand, defining the data and data types we want to store. But when this setup is done for a project like this, the benefits clearly outweigh the downsides.

Another approach for a database paradigm for this project could be a document database. It can store the data we want to store – both regarding users and todos. It is easily set up since a document database does not have any database schema. 
But since a document database does not have the concept of primary/foreign key relationship means that we will end up with a lot of redundant data in this project, which is not beneficial. For this project a document database iis simply not as good at solving the task as a relational database.



<br />

## Operating System Components
An operating system is a program that serves as an interface between the user of a computer and the hardware. It sets up an environment in which a user can run programs conveniently and efficiently. Examples of operating systems include DOS, UNIX, and Windows. 

<br />

**What influence does the OS have on the dev env?** 

We have used Alpine linux distro as an OS for the development environment.
Using this OS over other options came with the following benefits:

**Development:**
* lightweight - has no unnecessary dependencies packages
* big community of developers 
* commonly used on servers - less incompatibility issues in the deployment

**Deployment:**
* container friendly OS: lite package without unnecessary dependencies
* Nginx - stable and reliable on Alpine based images 

All members of the team have worked on a VPS with the same type of OS and same configurations. This made the project workflow very easy going and straightforward, by not hitting the cross platform barriers.

By having the same setup to work from when developing it ensured that the project related dependencies would behave the same since it was the same operating system. It also made it easier to share knowledge such as different Unix commands that we could run on the VPS, commands are different based on the OS, we didn’t have to worry about that issue since we all run the same OS. 

An example of a problem when using one OS in development and another in deployment environment is that the way of writing the path is different ( / \ ), this would cause a big problem since the server wouldn’t be able to point to the right files. 

<br />

## Why django:
Django is a robust python framework for developing web apps, It has been in the market a long time so it is a matured framework that has proven itself, because it has been out for so long, it has really been polished for many years it has a great ecosystem of third party apps around it. 
For our project, we need to handle the creation of users. Django has a built in user model, so validation of user data, and storing user passwords etc. is done in a more robust and secure way than writing a login/user system from scratch. 

Django also provides state of art security, normally a web app is vulnerable to many XSS attacks for example. Django offers built-in security against XSS attacks by providing a csrf token that gets sent whenever a form is submitted for example.
There is a healthy amount of django developers in the market, if our todo app would grow and become the number one todo-app in the market, then it would be easier to find a django developer than a Perl developer for example. 

Another benefit of the Django framework is the Object Relational Mapper. This handles the queries to the database. We don’t need to write raw SQL, but write our queries in Python/django. 



<br />
<br />
